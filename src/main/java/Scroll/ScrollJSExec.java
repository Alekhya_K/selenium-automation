package Scroll;

import javafx.scene.input.InputMethodTextRun;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.util.List;

public class ScrollJSExec {
    public static void main(String args[]){
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("window.scroll(0,500)");
        javascriptExecutor.executeScript("document.querySelector(\".tableFixHead\").scrollTop = 2000;");
        List<WebElement> tableRows = driver.findElements(By.xpath("//*[@class=\"right-align\"] //*[@id='product']/tbody/tr/td[4]"));
        int actualSum = 0;
        for(WebElement webElement: tableRows) {
            actualSum = actualSum + Integer.parseInt(webElement.getText());
        }
        System.out.println(actualSum);
        int expectedAmount = Integer.parseInt(driver.findElement(By.cssSelector(".totalAmount")).getText().split(":")[1].trim());
        Assert.assertEquals(actualSum,expectedAmount);
        
    }
 }
