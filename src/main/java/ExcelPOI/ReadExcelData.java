package ExcelPOI;

import GuruAutomation.Util;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Arrays;

public class ReadExcelData {
    public static void main(String[] args) throws IOException {
       getExcelData();
    }

    public static void getExcelData() throws IOException {
        //open file location
        File src = new File(Util.FILE_PATH);
        //read file
        FileInputStream inputStream = new FileInputStream(src);
        Workbook workbook = null;
        //find the file type
        String fileType = Util.FILE_PATH.substring(Util.FILE_PATH.indexOf("."));
        if(fileType.equals(".xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        }
        else if(fileType.equals(".xls")) {
            workbook = new HSSFWorkbook(inputStream);
        }

        //sheet name
        Sheet sheet = workbook.getSheet(Util.SHEET_NAME);

        //find number of rows in excel
        int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
        System.out.println("rowcount" +rowCount);

        Row row;
        String[][] data = new String[5][3];
        for(int i=1; i<rowCount; i++) {
            row = sheet.getRow(i);
            for(int j=1; j<row.getLastCellNum(); j++ ) {
                System.out.println(row.getCell(j).getStringCellValue() + "||");
                data[i][j] = row.getCell(j).getStringCellValue();
            }
            System.out.println();
        }
        System.out.println(Arrays.deepToString(data));

    }

}
