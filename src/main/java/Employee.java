public class Employee {

    String name;
    int empid;
    int sal;
    static int hike;

    public Employee(int empid, int sal,String name){
       this.empid = empid;
       this.sal = sal;
       this.name = name;
    }


    public static void main(String args[]) {
        Employee e1 = new Employee(101,200,"sfsd");
        Employee e2 = new Employee(102,34,"vishfd");
        hike = 10;

        System.out.println(hike);
    }

    public static String getEmpName(Employee e1) {
        return e1.name;
    }


}
