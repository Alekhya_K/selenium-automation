package testNG;

import GuruAutomation.Util;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class LoginPage {
    static WebDriver driver;
    String alertTitleActual;
    @BeforeSuite(alwaysRun = true)
    @Parameters({"BASE_URL"})
    public void setUp(String baseUrl) {
        System.setProperty("webdriver.chrome.driver", Util.WEBDRIVER_LOCATION); //location of chrome driver
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(baseUrl);
    }

    @Test(groups = {"Smoke"},dataProvider = "getData")
    public void loginValidation(String username, String password) {
        driver.findElement(By.name("uid")).sendKeys(username);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.xpath("//input[@value='LOGIN']")).click();

        try {
            Alert alert = driver.switchTo().alert();
            alertTitleActual = alert.getText();
            if (alertTitleActual.contains(Util.EXPECT_ERROR)) {
                System.out.println("test case passed");
                alert.accept();
            } else {
                System.out.println("test case failed");
            }
        } catch (NoAlertPresentException exception) {
            if (driver.getTitle().equals(Util.EXPECT_TITLE)) {
                System.out.println("test case passed");

                driver.navigate().back();
            } else {
                System.out.println("test case failed");
            }
        }
    }

    @AfterSuite(alwaysRun = true)
    public void endSession() {
        driver.quit();
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{{"mngr336503","vedejet"},{"valid","invalid"},{"invalid","valid"}};
    }

}
