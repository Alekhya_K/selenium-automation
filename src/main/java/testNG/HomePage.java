package testNG;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class HomePage {


    @Test
    public void welcomeMessage() {
       System.out.println("Welcome Message: " + LoginPage.driver.findElement(By.xpath("//tbody/tr[2]/td")).getText());
    }
    @Test
    public void getManagerId() {
        String managerId = LoginPage.driver.findElement(By.xpath("//tbody/tr[3]/td")).getText().split(":")[1].trim();
        System.out.println("Manager Id : " +managerId);
    }

}
