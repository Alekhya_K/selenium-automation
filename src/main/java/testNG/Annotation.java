package testNG;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Annotation {
    @Test
    public void setUp() {
        System.out.println("This is set up statement");
    }

    @Test
    public void hello() {
        System.out.println("Hello how are you");
    }

    @BeforeTest
    public void beforeAll() {
        System.out.println("This is executed before all");
    }
}
