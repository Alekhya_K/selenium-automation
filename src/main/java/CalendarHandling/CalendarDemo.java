package CalendarHandling;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class CalendarDemo {
    public static void main(String args[]) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.path2usa.com/travel-companions");
        driver.manage().window().maximize();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.id("travel_date")).click();
        Thread.sleep(5000);
        while (!driver.findElement(By.xpath("//div[@class='datepicker-days'] //*[@class='datepicker-switch']")).getText().toLowerCase().contains("oct")) {
            driver.findElement(By.xpath("//div[@class='datepicker-days'] //*[@class='next']")).click();
        }
        List<WebElement> days = driver.findElements(By.className("day"));
        for(WebElement webElement: days) {
            if(webElement.getText().trim().contains("5")) {
                webElement.click();
                break;
            }
        }
    }
}
