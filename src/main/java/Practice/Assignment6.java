package Practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

public class Assignment6 {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
        driver.manage().window().maximize();
        WebElement option = driver.findElement(By.xpath("//label[@for='benz']"));
        option.findElement(By.id("checkBoxOption2")).click();
        String selectedLabel = option.getText();
        WebElement dropDown = driver.findElement(By.id("dropdown-class-example"));
        Select select = new Select(dropDown);
        select.getOptions().forEach(i -> System.out.println(i.getText()));
        select.selectByVisibleText(selectedLabel.trim());
        driver.findElement(By.id("name")).sendKeys(selectedLabel);
        driver.findElement(By.id("alertbtn")).click();
        Assert.assertTrue(driver.switchTo().alert().getText().contains(selectedLabel));
    }
}
