package Practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class Assignment1CheckBoxes {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver chromeDriver = new ChromeDriver();
        chromeDriver.get("https://rahulshettyacademy.com/AutomationPractice/");
        //Step1
        chromeDriver.findElement(By.id("checkBoxOption1")).click();
        Assert.assertTrue(chromeDriver.findElement(By.id("checkBoxOption1")).isSelected());
        chromeDriver.findElement(By.id("checkBoxOption1")).click();
        Assert.assertFalse(chromeDriver.findElement(By.id("checkBoxOption1")).isSelected());
        //Step2
        System.out.println("Count of checkboxes: " +chromeDriver.findElements(By.cssSelector("input[type='checkbox']")).size());
    }
}
