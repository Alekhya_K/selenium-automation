package Practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Assignment2 {
    public static void main(String args[]) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.cleartrip.com/");
        driver.manage().window().maximize();

        //new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='flex flex-middle p-relative homeCalender'] //button"))).click();

       // driver.findElement(By.xpath("//div[@class='flex flex-middle p-relative homeCalender'] //button")).click();

            //Adults Dropdown
        WebElement AdultDropDown = driver.findElement(By.xpath("//div[@class='mb-4']/select"));
        Select selectAdult = new Select(AdultDropDown);
        selectAdult.selectByValue("5");
        //Children dropdown
        WebElement childDropDown = driver.findElement(By.xpath("//div[@class='mb-4']/ following-sibling::div[2]/select"));
        Select selectChildren = new Select(childDropDown);
        selectChildren.selectByValue("4");
        //Infants dropdown
        WebElement infantsDropdown = driver.findElement(By.xpath("//div[@class='mb-4']/ following-sibling::div[4]/select"));
        Select selectInfants = new Select(infantsDropdown);
        selectInfants.selectByValue("1");

        //moreoptions
        Thread.sleep(4000);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class=\"td-none hover:td-underline px-4 mt-1 weak\"]"))).click();
       // driver.findElement(By.xpath("//a[@class=\"td-none hover:td-underline px-4 mt-1 weak\"]")).click();
        WebElement moreOption1 = driver.findElement(By.xpath("//*[text()=\"Class of travel\"]/following-sibling::select"));
        Select classOfTravel = new Select(moreOption1);
        List<WebElement> options = classOfTravel.getOptions();
        for(WebElement option : options) {
            if(option.getText().equalsIgnoreCase("Business")) {
                classOfTravel.selectByValue("Business");
                break;
            }
        }

        //preferred airline
        driver.findElement(By.xpath("//body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[6]/div[1]/div[3]/div[1]/div[1]/div[1]/input[1]")).sendKeys("scoo");
        Thread.sleep(4000);
        List<WebElement> moreOption2 = driver.findElements(By.xpath("//*[text()='Preferred Airline']/following-sibling::div/div/div[2]/ul"));
        for(WebElement webElement: moreOption2) {
            if(webElement.getText().contains("Scoot")) {
                webElement.click();
                break;
            }
        }
        Thread.sleep(4000);
        //search
        driver.findElement(By.xpath("//button[text()='Search flights']")).click();

    }
}
