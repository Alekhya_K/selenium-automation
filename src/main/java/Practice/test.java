package Practice;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class test {


    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //System.out.println(driver.findElements(By.tagName("a")).size());
        System.out.println(driver.findElement(By.xpath("//div[@id='gf-BIG']")).findElements(By.tagName("a")).size());
        List<WebElement> footerList = driver.findElement(By.xpath("//div[@id='gf-BIG']")).findElements(By.tagName("a"));
        Actions actions = new Actions(driver);
        Set<String> windows;
        String parentWindow;
        String childWindow;

        Iterator<String> iterator;
        for(WebElement webElement: footerList) {
            actions.moveToElement(webElement).keyDown(Keys.CONTROL).click().build().perform();
        }
        windows = driver.getWindowHandles();
        iterator = windows.iterator();
        while(iterator.hasNext()) {
            driver.switchTo().window(iterator.next());
            System.out.println(driver.getTitle());
        }

        driver.quit();

    }


}
