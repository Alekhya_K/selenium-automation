package Practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Assignment8 {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        String expected = "";
        String actual = "United States (USA)";
        WebElement inputText = driver.findElement(By.xpath("//input[@id='autocomplete']"));
        inputText.sendKeys("Uni");
        List<WebElement> autoSuggestionList = driver.findElements(By.xpath("//ul[@id='ui-id-1']/li"));
        for(WebElement webElement: autoSuggestionList) {
            if(webElement.getText().trim().replace(" ","").toLowerCase().contains("unitedstates")) {
                expected = webElement.getText();
                webElement.click();
                break;
            }
        }
        Assert.assertEquals(expected, actual);
    }
}
