package Practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//Explicit Wait
public class Assignment3 {
    public static void main(String[] args) {
            System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
            WebDriver driver = new ChromeDriver();
            WebDriverWait explicitWait = new WebDriverWait(driver,5);
            driver.get("https://eps-uat.flyscootca.com/#/");
            driver.findElement(By.id("i0116")).sendKeys("cabin_initiator");


            driver.manage().window().maximize();
            driver.findElement(By.linkText("Click to load get data via Ajax!")).click();
            explicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("results")));
            System.out.println(driver.findElement(By.id("results")).getText());
            driver.close();

    }
}
