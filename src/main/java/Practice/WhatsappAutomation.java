package Practice;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
//
public class WhatsappAutomation {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://flipkart.com");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("._2KpZ6l._2doB4z")).click();
        driver.findElement(By.xpath("//*[@name=\"q\"]")).sendKeys("Apple Watches" + Keys.ENTER);
        String text = driver.findElement(By.xpath("//span[@class='_10Ermr']")).getText();
        int size = Integer.parseInt(text.trim().split("–")[1].split("of")[0].trim());
        List<WebElement> elements = driver.findElements(By.xpath("//*[@class='_1YokD2 _2GoDe3']/div[2]//div[@class='_1AtVbE col-12-12']"));
        int i=1;
        while(i <= size) {
            for (WebElement element : elements) {
                System.out.println(element.findElement(By.xpath("div//a//*[@class='_4rR01T']")).getText());
            }
            i++;
        }
        driver.close();


    }


}
