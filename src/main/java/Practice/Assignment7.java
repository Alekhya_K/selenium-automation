package Practice;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Assignment7 {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("window.scroll(0,600)");
        int rowCount = !driver.findElements(By.xpath("//table[@class=\"table-display\"]/tbody/tr")).isEmpty() ?
                driver.findElements(By.xpath("//table[@class=\"table-display\"]/tbody/tr")).size() : 0;
        int colCount = !driver.findElements(By.xpath("//table[@class=\"table-display\"]/tbody/tr/th")).isEmpty() ?
                driver.findElements(By.xpath("//table[@class=\"table-display\"]/tbody/tr/th")).size() : 0;
        System.out.println("No. of rows : "+ rowCount+"\n"+"No. of columns : " +colCount);
        List<WebElement> secondRow = driver.findElements(By.cssSelector(".table-display tr:nth-child(3) td"));
        System.out.println("Instructor : " +secondRow.get(0).getText() + "\nCourse : " +secondRow.get(1).getText() + "\nPrice : " +secondRow.get(2).getText());

    }
}
