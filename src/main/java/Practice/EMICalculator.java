package Practice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class EMICalculator {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://emicalculator.net/");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        Actions actions = new Actions(driver);
        WebElement homeLoan = driver.findElement(By.cssSelector("#loanamountslider > span"));
        WebElement interestRate = driver.findElement(By.cssSelector("#loaninterestslider > span"));
        WebElement loanTenure = driver.findElement(By.cssSelector("#loantermslider > span"));

        //drag and drop using x coordinates
        actions.dragAndDropBy(homeLoan,83,0).build().perform();
        actions.dragAndDropBy(interestRate,88,0).perform();
        //actions.dragAndDropBy(loanTenure,-120 ,0);
        actions.moveToElement(loanTenure).clickAndHold().moveByOffset(-111,0).release().perform();

        WebElement loanAmount = driver.findElement(By.cssSelector("#emiamount > p >span"));
        //Assert.assertEquals(loanAmount.getText(),"89");


    }
}
