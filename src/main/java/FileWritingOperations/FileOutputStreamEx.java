package FileWritingOperations;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamEx {
    public static void main(String args[]) throws IOException {

        String location = "UsingFileOutputStream.txt";
        String content = "Input using file output stream";

        FileOutputStream outputStream = new FileOutputStream(location);
        byte[] writethis = content.getBytes();
        outputStream.write(writethis);
        outputStream.close();

    }
}
