package FileWritingOperations;

import java.io.FileWriter;

public class FileWriteExample {
    public static void main(String args[])  {
        //1. file location
        //2. file content
        String path = "UsingFileWriter.txt";
        String content ="Java is interesting";
        try {
            FileWriter fileWriter = new FileWriter(path);
            fileWriter.write(content);
            fileWriter.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
}
