package FileWritingOperations;

import org.openqa.selenium.OutputType;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class BufferedWriterExample {
    public static void main(String args[]) throws IOException {
        String path = "UsingBufferedWriter.txt";
        String content = "Automation Testing";
        Writer writer = new FileWriter(path);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        bufferedWriter.write("Input using buffered writer");
        bufferedWriter.close();
    }
}
