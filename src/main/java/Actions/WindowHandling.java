package Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class WindowHandling {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://rahulshettyacademy.com/loginpagePractise/#");
        driver.findElement(By.cssSelector(".blinkingText")).click();
        Set<String> windows = driver.getWindowHandles(); //[parentid, child,subchild]
        Iterator<String> iterator = windows.iterator();
        String parentWindow =  iterator.next();
        String childWindow = iterator.next();
        driver.switchTo().window(childWindow);
        System.out.println(driver.findElement(By.cssSelector(".im-para.red")).getText());
        String[] list = driver.findElement(By.cssSelector(".im-para.red")).getText().split(" ");
        List<String> stringList = Arrays.asList(list);
        String mail =stringList.stream().filter(i -> i.contains("com")).findFirst().orElse("kalejhya1998@gmail.com");
        driver.switchTo().window(parentWindow);
        driver.findElement(By.id("username")).sendKeys(mail);




    }
}
