package RandomAssignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

//Automate End to End Buy Order functionality.
public class Assignment6 {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("http://automationpractice.com/index.php");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        System.out.println("Title of the Page: " +driver.getTitle());
        //hover over women link
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.xpath("//li //a[@title=\"Women\"]"))).build().perform();
        //click t-shirts
        driver.findElement(By.xpath("//a[@title=\"Women\"]/following-sibling::ul //a[@title=\"T-shirts\"]")).click();
        //hover on 1st item
        actions.moveToElement(driver.findElement(By.xpath("//div[@class='product-image-container']"))).build().perform();
        //click on more button
        driver.findElement(By.xpath("//a[@title=\"Add to cart\"]/following-sibling::a")).click();
        //increase the quantity to 2
        driver.findElement(By.xpath("//p[@id=\"quantity_wanted_p\"]/a[2]")).click();
        //select size L
        WebElement size = driver.findElement(By.id("group_1"));
        Select select = new Select(size);
        select.selectByVisibleText("L");
        //select color
        driver.findElement(By.xpath("//ul[@id=\"color_to_pick_list\"]/li[2]")).click();
        //click add to cart
        driver.findElement(By.xpath("//span[contains(text(),'Add to cart')]")).click();
       //click proceed to checkout
        driver.findElement(By.xpath("//a[@title=\"Proceed to checkout\"]")).click();
        //print total amount
        System.out.println("Total Price: " +driver.findElement(By.xpath("//td[@id='total_price_container']")).getText().trim());
        //proceed to checkout
        driver.findElement(By.xpath("//a[@title=\"Continue shopping\"]//preceding-sibling::a")).click();


    }
}
