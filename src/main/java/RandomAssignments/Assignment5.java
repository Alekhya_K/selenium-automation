package RandomAssignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Assignment5 {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://www.verizon.com/");
        driver.findElement(By.xpath("//button[@id='gnav20-sign-id']")).click();
        driver.findElement(By.xpath("//a[contains(text(),\"Sign in to My Account\")]")).click();
        driver.findElement(By.xpath("//input[@id=\"IDToken1\"]")).sendKeys("Alekhya");
        driver.findElement(By.id("IDToken2")).sendKeys("123");
        driver.findElement(By.id("rememberUserName")).click();
        driver.findElement(By.id("login-submit")).click();

    }
}
