package RandomAssignments;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class Assignment1 {
    public static void main(String args[]) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        //tc1
        driver.get("https://in.godaddy.com/");
        Dimension dimension = new Dimension(1920, 1080);
        driver.manage().window().setSize(dimension);
        Thread.sleep(200);
        System.out.println(driver.getTitle());
        System.out.println("title" +driver.findElement(By.tagName("title")));
        Assert.assertEquals(driver.getTitle(), driver.findElements(By.tagName("title")).get(0).getText());
        System.out.println(driver.getCurrentUrl());
        Assert.assertEquals(driver.getCurrentUrl(), "https://in.godaddy.com/");
        System.out.println(driver.getPageSource());
        driver.close();
    }
}
