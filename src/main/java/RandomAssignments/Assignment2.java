package RandomAssignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Assignment2 {
    public static void main(String args[]) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://www.amazon.in/");
        Thread.sleep(2000);
        driver.manage().window().maximize();
        String homePageTitle = driver.getTitle();
        System.out.println("Title" +homePageTitle);
        List<WebElement> menuLinks = driver.findElements(By.xpath("//div[@id='nav-xshop']/a"));
        menuLinks.forEach(i->System.out.println("listoflinks" +i.getText()));
        for (WebElement webElement: menuLinks) {
            System.out.println("text"+webElement.getText());
            try {
                webElement.click();

            }
            catch (org.openqa.selenium.StaleElementReferenceException ex){
                webElement.click();
            }
            System.out.println("Page title" +driver.getTitle());
            driver.navigate().back();
            Thread.sleep(2000);
            /*if(homePageTitle.equals(driver.getTitle())) {
                System.out.println("Menu Link Test Passed for: " +webElement.getText());
            }*/
        }
    }
}
