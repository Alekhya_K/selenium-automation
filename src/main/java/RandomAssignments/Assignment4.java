package RandomAssignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

//google search automation  - In Progress
public class Assignment4 {
    public static void main(String args[]) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://www.google.com/"); //div[@role="option"] /div/span
        driver.findElement(By.xpath("//input[@role=\"combobox\"]")).sendKeys("automation");
        Thread.sleep(5000);
        List<WebElement> autoSuggestions = driver.findElements(By.xpath("//div[@role=\"option\"]"));
        autoSuggestions.stream().forEach(i -> System.out.println(i.findElement(By.xpath("//div[1]/span/b")).getText()));
    }
}
