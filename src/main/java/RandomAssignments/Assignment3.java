package RandomAssignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

//login form automation
public class Assignment3 {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://www.techlistic.com/p/selenium-practice-form.html");
        driver.findElement(By.name("firstname")).sendKeys("Alekhya");
        driver.findElement(By.name("lastname")).sendKeys("K");
        driver.findElement(By.xpath("//div[@class=\"control-group\"] //label[contains(text(),'Female')] //preceding-sibling::input[1]")).click();
        driver.findElement(By.cssSelector("input[value=\"2\"]")).click();
        driver.findElement(By.id("datepicker")).sendKeys(LocalDate.now().toString());
        WebElement continent = driver.findElement(By.id("continents"));
        Select select = new Select(continent);
        select.selectByVisibleText("Asia");
        //upload file
        driver.findElement(By.id("photo")).sendKeys("C:\\Users\\1646238\\Downloads\\test.jpg");


    }
}
