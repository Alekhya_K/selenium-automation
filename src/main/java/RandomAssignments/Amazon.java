package RandomAssignments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

//covering most of the concepts in selenium - Own Assignment 1
public class Amazon {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //step 1
        driver.get("https://www.amazon.in/");
        driver.manage().window().maximize();
        //step 2
        System.out.println("Home Page Title" +driver.getTitle());
        //step 3
        WebElement allDropdown = driver.findElement(By.id("searchDropdownBox"));
        Select select = new Select(allDropdown);
        select.selectByVisibleText("Amazon Fashion");
        //step 4
        driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']")).sendKeys("Women");
        List<WebElement> autoSuggestWomen = driver.findElements(By.xpath("//div[@id='suggestions']/div"));
        autoSuggestWomen.forEach(i->System.out.println(i.getText()));
        autoSuggestWomen.stream().filter(i -> i.getText().toLowerCase().contains("kurta")).findFirst().get().click();
        //step 5
        Actions womenHover = new Actions(driver);
        womenHover.moveToElement(driver.findElement(By.xpath("//div[@id='nav-subnav'] //a//span[contains(text(),'Women')]"))).build().perform();
        womenHover.moveToElement(driver.findElement(By.xpath("//a[contains(text(),'New Arrivals')]"))).click().build().perform();
        //step 6
        driver.findElement(By.xpath("//span[@id='a-autoid-0-announce']")).click();
        List<WebElement> sortByList = driver.findElements(By.xpath("//div[@class='a-popover-inner']/ul/li"));
        for(WebElement webElement:sortByList){
            if(webElement.findElement(By.tagName("a")).getText().toLowerCase().contains("low to high")) {
                webElement.findElement(By.tagName("a")).click();
                break;
            }
        }
        //step 7
        List<WebElement> sortedList = driver.findElements(By.cssSelector(".a-price-whole"));
        sortedList.forEach(i -> System.out.println(i.getText()));
        for(int i=0; i<sortedList.size()-2; i++) {
            if(Integer.parseInt(sortedList.get(i).getText().replace(",","")) > Integer.parseInt(sortedList.get(i+1).getText().replace(",",""))) {
                System.out.println(sortedList.get(i).getText() + " " +sortedList.get(i+1).getText());
            }
        }
        //Assert.assertTrue(test,"Low to High Sorted List test case passed");

        //step 8
        //span[@class='a-price-whole']/../../../../../preceding-sibling::div[@class="a-section a-spacing-none a-spacing-top-small"]/h2/a

        for (WebElement webElement : sortedList) {
            if (Integer.parseInt(webElement.getText()) > 100 && Integer.parseInt(webElement.getText()) < 200) {
                System.out.println("price" + webElement.getText());
                System.out.println("link" + webElement.findElement(By.xpath("//span[@class='a-price-whole']/../../../../../preceding-sibling::div[@class=\"a-section a-spacing-none a-spacing-top-small\"]/h2/a")).getText());
                webElement.findElement(By.xpath("../../../../../preceding-sibling::div[@class=\"a-section a-spacing-none a-spacing-top-small\"]/h2/a")).click();
                break;
            }
        }

        //step 9- window handling
        Set<String> windows = driver.getWindowHandles();
        Iterator<String> iterator = windows.iterator();
        String parentWindow = iterator.next();
        String childWindow = iterator.next();
        driver.switchTo().window(childWindow);
        String prodName = driver.findElement(By.xpath("//span[@id='productTitle']")).getText().trim();
        WebElement addToCart = driver.findElement(By.id("add-to-cart-button"));
        System.out.println("Add to cart" +addToCart.getAttribute("style"));
        boolean exists = true;
        if(addToCart.getAttribute("style").contains("not-allowed")) {
            WebElement sizeDropDown = driver.findElement(By.id("native_dropdown_selected_size_name"));
            Select select1 = new Select(sizeDropDown);
            select1.getOptions().forEach(i -> i.getText().trim());
            select1.selectByVisibleText("M");
            Thread.sleep(3000);
            exists = driver.findElements( By.id("outOfStock") ).size() == 0;
            if (!exists) {
                System.out.println("Item currently unavailable");
                driver.quit();
            }
        }
        if(exists) {
            System.out.println("inside");
            driver.findElement(By.id("add-to-cart-button")).click();
            driver.findElement(By.xpath("//a[@id='hlb-view-cart-announce']")).click();
            //List<WebElement> cartItems = //div[@data-name="Active Items"]//div[@class="a-row sc-list-item sc-list-item-border sc-java-remote-feature"][1]/div//span[@class="a-truncate-cut"]
            /*List<WebElement> cartItems = driver.findElements(By.xpath("//div[@data-name=\"Active Items\"]//div[@class=\"a-row sc-list-item sc-list-item-border sc-java-remote-feature\"]"));
            cartItems.stream().filter(i -> i.findElement(By.xpath("//div//span[@class=\"a-truncate-cut\"]")).getText().contains(prodName)).forEach(i -> System.out.println("Add to cart functionality passed"));
            */
            System.out.println("Product name" +prodName);
            List<WebElement> temp = driver.findElements(By.xpath("//div[@data-name=\"Active Items\"]//span[@class='a-list-item']/a/span[1]"));
            temp.forEach(i->System.out.println(i.getText()));
            temp.stream().filter(i -> i.getText().trim().contains(prodName)).forEach(i->System.out.println("Add to cart passed"));
        }
    }
}
