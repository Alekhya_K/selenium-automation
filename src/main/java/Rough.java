import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Rough {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        //Step1 :create driver object
        WebDriver chromeDriver = new ChromeDriver();
        chromeDriver.get("https://git.flyscootca.com/users/sign_in");
        chromeDriver.findElement(By.xpath("//*[@value='Continue']")).click();
        WebElement e = chromeDriver.findElement(By.id("user_login"));
        e.sendKeys("Alekhya");
        Actions actions = new Actions(chromeDriver);
        actions.moveToElement(e).doubleClick().keyDown(Keys.CONTROL).build().perform();
        chromeDriver.findElement(By.id("user_password")).sendKeys(Keys.CONTROL+"V");

    }
}
