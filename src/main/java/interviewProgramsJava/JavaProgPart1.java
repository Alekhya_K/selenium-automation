package interviewProgramsJava;

import java.util.*;
import java.util.stream.Collectors;

public class JavaProgPart1 {

    public static void main(String args[]) {
      Scanner sc  = new Scanner(System.in);
      int n = sc.nextInt();
      switchWithoutBreak(n);
      //System.out.println(isprime(n));
      //System.out.println(findFactorial(n));
       // System.out.println(reverseNumber(n));

       //String userInput1 = sc.nextLine();
       //String userInput2 = sc.nextLine();
       // System.out.println(isPalindrome(userInput));

        //System.out.println(filterDuplicateElements(Arrays.asList(1,2,5,4,2,4,4,2,1,0,3,2,0)));

        //System.out.println(wordsReverse("This is , me"));
        //starPattern(3);
        //System.out.println(isAnagram(userInput1,userInput2));
        //System.out.println(charOccurence("aakulbessanu"));

        //System.out.println(removeDuplicateChars("Mynameisalekhya"));
        //List<Integer> integerList = Arrays.asList(16,45,1,0,2);
        //System.out.println(sortList(integerList));
    }

    //number is prime or not?
    public static Boolean isprime(int n) {
        boolean isPrime = true;
        for(int i=2; i<n ;i++) {
            if(n%i == 0) {
                isPrime = false;
                break;
            }
        }
        return isPrime;
    }

    public static int findFactorial(int n) {
        if(n==1) {
            return 1;
        }
        else {
            return n * findFactorial(n-1);
        }
    }

    //reverse a number
    public static int reverseNumber(int n) {
        String temp = ((Integer)n).toString();
        String res = "";
        for(int i=temp.length()-1 ; i>=0 ;i--) {
            res = res + temp.charAt(i);
        }
        System.out.println(res);
        return Integer.parseInt(res);

    }

    public static boolean isPalindrome(String input) {

        String reverse = "";

        for(int i= input.length()-1; i>=0;i--) {

            reverse = reverse + input.charAt(i);

        }

        if(reverse.equals(input)) return true;
        return false;


    }

    public static int countTrue(boolean[] arr) {

        int count =0;
        for(int i=0; i<arr.length; i++) {
            if(arr[i]) {
                count = count +1;
            }
        }
        return count;
    }

    public static  List<Integer> filterDuplicateElements(List<Integer> integerList) {

        List<Integer> result = new ArrayList<>();

        for(Integer i: integerList) {
            if(!result.contains(i)) {
                result.add(i);
            }
        }
        return result;

    }

    public static String wordsReverse(String s) {
        String[] beforeReverse = s.split(" ");
        StringBuilder reversedString ;
        List<String> afterReverse = new ArrayList<>();
        for(int i=0 ;i<beforeReverse.length; i++) {
            if(beforeReverse[i].length()>1) {
                    reversedString = new StringBuilder(beforeReverse[i]);
                    afterReverse.add(reversedString.reverse().toString());
            }
            else {
                afterReverse.add(beforeReverse[i]);
            }
        }

        return String.join(" ", afterReverse);

    }

    public static void starPattern(int num) {
        for(int i=1; i<=num; i++) {
            for(int j=1; j<=i ;j++) {
                System.out.print("*");
            }
            System.out.println("\n");
        }
    }

    public static boolean isAnagram(String str1, String str2) {
        boolean b = true;
        char[] ch2 = str2.toLowerCase().replace(" ","").replaceAll("[^a-zA-Z0-9]","").toCharArray();
        for(int i=0; i<ch2.length; i++) {
            if(!str1.toLowerCase().contains(String.valueOf(ch2[i]))) {
                b = false;
            }
        }
        return b;
    }

    //find occurences of character in a string

    public static Map<Character,Integer> charOccurence(String str) {
        Map<Character,Integer> map = new HashMap<>();
        for(int i=0; i<str.length(); i++) {

            if(!map.containsKey(str.charAt(i))) {
                map.put(str.charAt(i),1);
            }
            else {
                map.put(str.charAt(i),map.get(str.charAt(i))+1);
            }
        }
        return map;
    }

    //remove duplicate chars from a string

    public static String removeDuplicateChars (String str) {
        StringBuilder res= new StringBuilder();
        for(int i=0; i<str.length();i++) {
            if(!res.toString().contains(String.valueOf(str.charAt(i)))) {
                res.append(str.charAt(i)); //res = res + String.valueOf(str.charAt(i))
            }
        }
        return res.toString();

    }

    public static int strlength(String str) {

        char[] chars = str.toCharArray();
        int length =0;

        for(char c:chars) {
            length++;
        }
        return length;


    }

    public static String retrieveThroughIndex(List<String> stringList, int index) {
        return stringList.get(index);
    }

    public static List<Integer> sortList(List<Integer> integerList) {
        Collections.sort(integerList);//ascending order
        Collections.reverse(integerList);//descending order
        // Collections.copy(List1, List2); to copy one list to another
        //Collections.shuffle(list_Strings); to shuffle the elements of the list
        //list_Strings.subList(0, 3); to extract portion of list. returns 3 elements from 0th index
        //         Collections.swap(c1, 0, 2); swap the first index element with 3rd index element
        //          c1.removeAll(c1); empty array list
        //      //Removing 1st element -        //       al.remove(0);
        //Collections.sort(arraylist, Collections.reverseOrder()); - sorting in revese order
        return integerList;
    }

    public static void switchWithoutBreak(int i) {
        switch (i) {
            case 0:
                System.out.println("I'm case 0");
            case 1:
                System.out.println("I'm case 1");
            case 2:
                System.out.println("I'm case 2");
            default:System.out.println("default");
        }

    }






}
