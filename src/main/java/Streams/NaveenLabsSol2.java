package Streams;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class NaveenLabsSol2 {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.noon.com/uae-en/");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //product names
        //h3[contains(text(),'Recommended for you')]/../../following-sibling::div/div[1] //div[@data-qa="product-name"/span/span]

        //next
        //h3[contains(text(),'Recommended for you')]/../../following-sibling::div/div[2]
        List<WebElement> recommendForYou = driver.findElements(By.xpath("//h3[contains(text(),'Recommended for you')]/../../following-sibling::div/div[1] //div[@data-qa=\"product-name\"]"));
        recommendForYou.stream().forEach(i -> System.out.println(i.getText()));
    }
}
