package Streams;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

//webtable sort and pagination
public class WebTableSort {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://rahulshettyacademy.com/seleniumPractise/#/offers");
        List<WebElement> list = driver.findElements(By.cssSelector("tr td:nth-child(1)"));
        /*List<String> originalList = list.stream().map(s -> s.getText()).collect(Collectors.toList());
        List<String> sortedList = originalList.stream().sorted().collect(Collectors.toList());
        //Assert.assertEquals(originalList,sortedList);

        Map<String, String> map= new HashMap<>();
        List<String> names = driver.findElements(By.xpath("//table[@class=\"table table-bordered\"] //tr/td[1]")).stream().map(WebElement::getText).collect(Collectors.toList());
        List<String> prices = driver.findElements(By.xpath("//table[@class=\"table table-bordered\"] //tr/td[2]")).stream().map(WebElement::getText).collect(Collectors.toList());

        for(int i=0; i<names.size(); i++) {
            map.put(names.get(i),prices.get(i));
        }

        map.entrySet().forEach( i ->{
                System.out.println(i.getKey() +"-" +i.getValue());
        });*/

        List<String> list1;
         do {
            List<WebElement> temp = driver.findElements(By.xpath("//table[@class=\"table table-bordered\"] //tr/td[1]"));
            list1 = temp.stream().filter(i -> {
                return i.getText().equals("Guava");
            }).map(i -> i.findElement(By.xpath("following-sibling::td[1]")).getText()).collect(Collectors.toList());
            System.out.println(list1);

            if(list1.isEmpty()) {
                driver.findElement(By.xpath("//a[@aria-label=\"Next\"]")).click();

            }
        }while (list1.isEmpty());


        driver.findElement(By.id("search-field")).sendKeys("orange");
        List<WebElement> expectedList = driver.findElements(By.xpath("//table[@class=\"table table-bordered\"] //tr/td[1]"));
        List<WebElement> actualList = expectedList.stream().filter(i->i.getText().contains("Orange")).collect(Collectors.toList());
        Assert.assertEquals(actualList,expectedList);
    }

}
