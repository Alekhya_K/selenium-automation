package Streams;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class x {
    static WebDriver driver;
 public static void main(String args[]) throws InterruptedException {

     String XPATH = "//div[@class='maincounter-number']//span[@class='rts-counter']";
     String XPATH1 = "//div[@class=\"sec-counter\"] //span[@class=\"rts-counter\"]";
     System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe"); //location of chrome driver
     driver = new ChromeDriver();
     driver.get("https://www.worldometers.info/world-population/");
     Thread.sleep(5000);
     populateData(XPATH,XPATH1);
 }

     public static void populateData(String XPATH,String XPATH1) throws InterruptedException {
     while (true) {
         WebElement element = driver.findElement(By.xpath(XPATH));
         System.out.println(element.getText());
         List<WebElement> elementList = driver.findElements(By.xpath(XPATH1));
         elementList.stream().forEach(i -> System.out.println(i.getText()));
         Thread.sleep(10000);
     }
 }
}
