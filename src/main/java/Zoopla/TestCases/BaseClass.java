package Zoopla.TestCases;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseClass {

    public static WebDriver driver = null;
    Properties properties = null;
    Logger logger = Logger.getLogger(BaseClass.class);

    public void loadProperty() {
        try {
            FileInputStream inputStream = new FileInputStream("config.properties");
            properties = new Properties();
            properties.load(inputStream);

        } catch (IOException e ){
            e.printStackTrace();
        }
    }

    @BeforeSuite
    public void setUp() {
        PropertyConfigurator.configure("log4j.properties");
        loadProperty();
        if(properties.getProperty("browser").equals("Chrome")) {
            System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");
            driver = new ChromeDriver();
            logger.info("Launching the chrome browser");

        }
        logger.info("Navigating to the url");
        driver.get(properties.getProperty("url"));
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[contains(text(),'Save my preferences')]")).click();
    }

    @AfterSuite(enabled = false)
    public void tearDown() {
        logger.info("Closing the browser");
        driver.close();
    }

}
