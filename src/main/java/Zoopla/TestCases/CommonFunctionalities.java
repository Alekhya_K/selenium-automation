package Zoopla.TestCases;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class CommonFunctionalities {

    public  void sendKeys(WebElement element, String keys) {
        element.sendKeys(keys);
    }

    public  void onClick(WebElement element) {
        element.click();
    }

    public void onClickNewTab(WebElement element) {
        String keyString =   Keys.CONTROL+Keys.ENTER.toString();
        element.sendKeys(keyString);
    }

    public  String getText(WebElement element) {
        return element.getText();
    }

}
