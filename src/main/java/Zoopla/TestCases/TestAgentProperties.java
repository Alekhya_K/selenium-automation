package Zoopla.TestCases;

import Zoopla.PageObjects.HomePageObject;
import Zoopla.PageObjects.ResultPageObject;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestAgentProperties extends BaseClass {

    String actualAgentName;
    String expectedAgentName;

    @Test
    public void testAgentProperties() {
        HomePageObject pageObject = new HomePageObject(driver);
        ResultPageObject resultPageObject = new ResultPageObject(driver);
        pageObject.enterAndSearch();
        pageObject.getWebElementList();
        pageObject.getPrices();
        pageObject.clickNthElement(5);
        actualAgentName = resultPageObject.getAgentName(resultPageObject.agentName);
        resultPageObject.clickAgentProperties();
        logger.info("Viewing the agent details");
        expectedAgentName = driver.findElement(By.xpath("//h1/a")).getText();
        Assert.assertTrue(expectedAgentName.contains(actualAgentName));
    }
}
