package Zoopla.utilities;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

import lombok.SneakyThrows;
import org.testng.ITest;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import com.aventstack.extentreports.*;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class Reporting extends TestListenerAdapter{

    public ExtentReports extentReport;
    public ExtentSparkReporter sparkReporter;
    public ExtentTest logger;

    @SneakyThrows
    public void onStart(ITestContext testContext) {


        String timeStamp = LocalDate.now().toString();
        String repName = "Test-Report-" + timeStamp+".html";
        sparkReporter = new ExtentSparkReporter(System.getProperty("user.dir")+"\\test-output\\"+repName); //specify location
        sparkReporter.loadXMLConfig(System.getProperty("user.dir")+"\\testng.xml");

        extentReport = new ExtentReports();

        extentReport.attachReporter(sparkReporter);
        extentReport.setSystemInfo("host name","localhost");
        extentReport.setSystemInfo("user", "Alekhya");

        sparkReporter.config().setDocumentTitle("Bank Proj Testing");
        sparkReporter.config().setReportName("Functional Testing");

    }


    public void onTestSuccess(ITestResult tr) {
        logger = extentReport.createTest(tr.getName()); //create new entry in the report
        logger.log(Status.PASS, MarkupHelper.createLabel(tr.getName(), ExtentColor.GREEN)); //send the passed information


    }

    public void onTestFailure(ITestResult tr) {
        logger = extentReport.createTest(tr.getName()); //create new entry in the report
        logger.log(Status.FAIL, MarkupHelper.createLabel(tr.getName(), ExtentColor.RED));

        String screenshotPath = System.getProperty("user.dir") + "\\Screenshot\\" + tr.getName() + ".png";
        File file = new File(screenshotPath);

        if(file.exists()) {
            try {
                logger.fail("Screenshot is below" +logger.addScreenCaptureFromPath(screenshotPath));
            }
            catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }

    }

    public void onTestSkipped(ITestResult tr) {
        logger = extentReport.createTest(tr.getName()); //create new entry in the report
        logger.log(Status.SKIP, MarkupHelper.createLabel(tr.getName(), ExtentColor.ORANGE)); //send the passed information


    }

    public void onFinish(ITestContext iTestContext) {
        extentReport.flush();
    }



}
