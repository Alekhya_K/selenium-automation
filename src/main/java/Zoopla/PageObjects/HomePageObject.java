package Zoopla.PageObjects;

import Zoopla.TestCases.CommonFunctionalities;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.*;
import java.util.stream.Collectors;

public class HomePageObject {

    WebDriver driver;
    List<WebElement> webElementList = new ArrayList<>();
    List<Integer> finalPrices = new ArrayList<>();
    List<String> priceList = new ArrayList<>();
    Logger logger = Logger.getLogger(HomePageObject.class);


    private final CommonFunctionalities commonClass = new CommonFunctionalities();

    @FindBy(id = "header-location")
    public WebElement searchArea;

    @FindBy(xpath = "//button[@data-testid='search-button']")
    public WebElement searchBtn;

    @FindBy(xpath = "//div[@class=\"css-kdnpqc-ListingsContainer earci3d2\"]")
    public WebElement listofElements;

    public HomePageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public  void enterAndSearch() {
        logger.info("Entered the enterAndSearch() Method");
        logger.info("Entering the details");
        commonClass.sendKeys(searchArea,"London");
        commonClass.onClick(searchBtn);
    }

    public void getWebElementList() {
        logger.info("Entered the getWebElementList() Method");
        webElementList = listofElements.findElements(By.xpath("div"));
    }

    public void getPrices() {
        logger.info("Entered the getPrices() Method");
        webElementList.stream().forEach(System.out::println);

        for(int i=1; i<webElementList.size()-1; i++) {
            priceList.add(listofElements.findElement(By.xpath("div["+i+"]//div[@data-testid='listing-price'] //p[contains(text(),'£')]")).getText());
        }

        priceList.forEach(System.out::println);
        convertStringPriceToIntPrice(priceList);
        logger.info("Getting the prices of the items in the descending order");
    }

    public void convertStringPriceToIntPrice(List<String> priceList) {
        finalPrices = priceList.stream().map(i -> Integer.parseInt(i.replace("£","").replace(",","").trim())).collect(Collectors.toList());
        finalPrices.sort(Comparator.reverseOrder());
        System.out.println(finalPrices);
    }

    public void clickNthElement(int n) {
        logger.info("Entered the clickNthElement() Method");
        logger.info("Clicking on the element" +n);
        commonClass.onClick(webElementList.get(n));
    }


}
