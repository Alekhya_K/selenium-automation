package Zoopla.PageObjects;

import Zoopla.TestCases.BaseClass;
import Zoopla.TestCases.CommonFunctionalities;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class ResultPageObject{

    WebDriver driver;
    private final CommonFunctionalities commonClass = new CommonFunctionalities();
    Logger logger = Logger.getLogger(BaseClass.class);

    @FindBy(xpath = "//div[@data-testid='agent-details']//p")
    public WebElement agentName;

    @FindBy(xpath = "//div[@data-testid='agent-details']//p/a")
    public WebElement viewAgentProperties;

    public ResultPageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public String getAgentName(WebElement agentName) {
        return commonClass.getText(agentName).replaceAll("\nView agent properties","").trim();
    }

    public void clickAgentProperties() {
        logger.info("Entered the clickAgentProperties() Method");
        logger.info("Clicking on the view agent details");
        commonClass.onClick(viewAgentProperties);
    }



}
