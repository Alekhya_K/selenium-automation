package GuruAutomation.TestCases;

import GuruAutomation.ObjectRepository.ManagerHomePage;
import GuruAutomation.ObjectRepository.LoginPagePageFactory;
import GuruAutomation.Util;
import jxl.read.biff.BiffException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

//mngr336503
//vedejet
public class BankProject {
    public static WebDriver driver;
    String alertTitleActual;
    @BeforeSuite(alwaysRun = true)
    @Parameters({"BASE_URL"})
    public static void setUp(String baseUrl) {
        System.setProperty("webdriver.chrome.driver", Util.WEBDRIVER_LOCATION); //location of chrome driver
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(baseUrl);
    }

    /**
     * Above test script executed several times for each set of data used in @DataProvider
     * annotation. Any failed test does not impact other set of execution.
     *
     * SS1: Enter valid userid & password
     * Expected: Login successful home page shown
     * SS2: Enter invalid userid & valid password
     * SS3: Enter valid userid & invalid password
     * SS4: Enter invalid userid & invalid password
     * Expected:
     * A pop-up “User or Password is not valid” is shown
     *
     * @param username
     * @param password
     * @throws Exception
     */
    @Test(dataProvider = "getData")
    public void loginValidation(String username, String password) throws IOException {
        LoginPagePageFactory loginPagePageFactory = new LoginPagePageFactory(driver);
        ManagerHomePage managerHomePage = new ManagerHomePage(driver);
        System.out.println("username" +username +"password" +password);
        loginPagePageFactory.getUserID().sendKeys(username);
        loginPagePageFactory.getPassword().sendKeys(password);
        loginPagePageFactory.getLoginButton().click();

        try { //SS2,SS3,SS4
            Alert alert = driver.switchTo().alert();
            alertTitleActual = alert.getText();
            if (alertTitleActual.contains(Util.EXPECT_ERROR)) {
                System.out.println("test case passed");
                alert.accept();
            } else {
                System.out.println("test case failed");
            }
        } catch (NoAlertPresentException exception) {  //SS1
            if (driver.getTitle().equals(Util.EXPECT_TITLE)) {
                String actualManagerId = managerHomePage.getManagerId().getText().split(":")[1].trim();
                Assert.assertEquals(actualManagerId, Util.EXPECT_MANAGERID);
                Util.getScreenShot(driver);
                System.out.println("test case passed");
                driver.navigate().back();
            } else {
                System.out.println("test case failed");
            }
        }
    }


    @DataProvider  //dataprovider must return object or iterator of object
    public Object[][] getData() throws BiffException, IOException {
        return Util.getDataFromExcel(Util.FILE_PATH, Util.SHEET_NAME, Util.TABLE_NAME);
    }

   /* @AfterSuite(alwaysRun = true)
    public void sessionClose() {
        driver.quit();
    }*/

}
