package GuruAutomation.TestCases;

import GuruAutomation.ObjectRepository.*;
import GuruAutomation.Util;
import org.apache.commons.exec.util.MapUtils;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManagerOperations{

    Map<String, String> customerDetail = new HashMap<>();
    Map<String, String> accountDetail = new HashMap<>();

    @Test
    public void LoginSuccessful() {
        LoginPagePageFactory loginPagePageFactory = new LoginPagePageFactory(BankProject.driver);
        loginPagePageFactory.getUserID().sendKeys(Util.USERNAME);
        loginPagePageFactory.getPassword().sendKeys(Util.PASSWORD);
        loginPagePageFactory.getLoginButton().click();
    }

    //SM4
    @Test(dependsOnMethods = "LoginSuccessful",enabled = false)
    public void addNewCustomer() {
        ManagerHomePage managerHomePage = new ManagerHomePage(BankProject.driver);
        AddCustomerPage customerPage = new AddCustomerPage(BankProject.driver);
        managerHomePage.addNewCustomer().click();
        if (customerPage.getHeading().isDisplayed()) {
            customerPage.getCustomerName().sendKeys("Virendra");
            customerPage.getMaleGender().click();
            customerPage.getDob().sendKeys("15-02-1998");
            customerPage.getAddress().sendKeys("Jamnagar");
            customerPage.getCity().sendKeys("Jamnagar");
            customerPage.getState().sendKeys("Gujarat");
            customerPage.getPinNo().sendKeys("567321");
            customerPage.getPhoneNo().sendKeys("8000439024");
            customerPage.getEmailId().sendKeys("akhar@gmail.com");
            customerPage.getPassword().sendKeys("1234");
            customerPage.getSubmit().click();
        }
        try {
            Alert alert = BankProject.driver.switchTo().alert();
            System.out.println("Alert Message: " + alert.getText());
        } catch (NoAlertPresentException exception) {
            //cid - 63872
            if (customerPage.getHeading().getText().equals(Util.SUCCESSFUL_REGISTRATION)) {
                System.out.println("Registered Customer details are as follows:");
                List<WebElement> rowsList = BankProject.driver.findElements(By.xpath("//table[@id=\"customer\"]//tr"));
                for (int i = 3; i < rowsList.size() - 1; i++) {
                    List<WebElement> rowDetail = rowsList.get(i).findElements(By.xpath("td"));
                    customerDetail.put(rowDetail.get(0).getText(), rowDetail.get(1).getText());
                }
            }
            customerDetail.forEach((key, value) -> System.out.println(key + ": " + value));
            System.out.println("Customer Account Successfully created");
            BankProject.driver.findElement(By.linkText("Continue")).click();
            if (BankProject.driver.getTitle().equals(Util.EXPECT_TITLE)) {
                System.out.println("Test Executed Successfully");
            }
        }

    }

    //SM5
    @Test(dependsOnMethods = "addNewCustomer",enabled = false)
    public void addNewAccount(){
        String customerId;
        AddAccount addAccount = new AddAccount(BankProject.driver);
        ManagerHomePage homePage = new ManagerHomePage(BankProject.driver);
        homePage.getNewAccount().click();
        customerId = customerDetail.entrySet().stream().filter(i -> i.getKey().replaceAll(" ","").toLowerCase().contains("customerid")).map(i -> i.getValue()).findFirst().get();
        System.out.println("customerId" +customerId);
        System.out.println(addAccount.getHeading().getText());
        if(addAccount.getHeading().getText().equals(Util.ADD_NEW_ACCOUNT)) {
            addAccount.getCustomerId().sendKeys(customerId);
            addAccount.adminViaDropDown(0);
            addAccount.getInitialDeposit().sendKeys("500");
            addAccount.getSubmit().click();
        }
        try {
            Alert alert = BankProject.driver.switchTo().alert();
            System.out.println("Alert Message: " + alert.getText());
        }
        catch (NoAlertPresentException exception) {
            if (addAccount.getHeading().getText().equals(Util.ACCOUNT_GENERATION_SUCCESS)) {
                System.out.println("Registered Account details are as follows:");
                List<WebElement> rowsList = BankProject.driver.findElements(By.xpath("//table[@id=\"account\"]//tr"));
                for (int i = 3; i < rowsList.size() - 1; i++) {
                    List<WebElement> rowDetail = rowsList.get(i).findElements(By.xpath("td"));
                    accountDetail.put(rowDetail.get(0).getText(), rowDetail.get(1).getText());
                }
            }
        }
        accountDetail.forEach((key, value) -> System.out.println(key + ": " + value));
        System.out.println("Account Successfully generated");
        BankProject.driver.findElement(By.linkText("Continue")).click();
        if (BankProject.driver.getTitle().equals(Util.EXPECT_TITLE)) {
            System.out.println("Test Executed Successfully");
        }
    }

    //SM6
    @Test
    public void deleteAccount() throws IOException {
        ManagerHomePage homePage = new ManagerHomePage(BankProject.driver);
        DeleteAccountPage deleteAccountPage = new DeleteAccountPage(BankProject.driver);
        homePage.getDeleteAccount().click();
        if(deleteAccountPage.getHeading().getText().equals(Util.DELETE_ACCOUNT_HEADING)) {
            deleteAccountPage.getAccountNo().sendKeys("94498");
            deleteAccountPage.getSubmit().click();
        }
        try {
            Alert alert = BankProject.driver.switchTo().alert();
            if(alert.getText().equals("Do you really want to delete this Account?")) {
                alert.accept();
            }
            alert.accept();
        }
        catch (NoAlertPresentException exception) {
              Util.getScreenShot(BankProject.driver);
        }

    }
}
