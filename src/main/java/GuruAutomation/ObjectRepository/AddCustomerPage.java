package GuruAutomation.ObjectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddCustomerPage {
    WebDriver driver;

    public AddCustomerPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "heading3")
    WebElement heading;

    @FindBy(xpath = "//td[contains(text(),'Customer Name')]/following-sibling::td/input")
    WebElement customerName;

    @FindBy(xpath = "//td[contains(text(),'Gender')]/following-sibling::td/input[1]")
    WebElement maleGender;

    @FindBy(xpath = "//td[contains(text(),'Gender')]/following-sibling::td/input[2]")
    WebElement femaleGender;

    @FindBy(id = "dob")
    WebElement dob;

    @FindBy(xpath = "//textarea[@name=\"addr\"]")
    WebElement address;

    @FindBy(xpath = "//input[@name=\"city\"]")
    WebElement city;

    @FindBy(xpath = "//input[@name=\"state\"]")
    WebElement state;

    @FindBy(xpath = "//input[@name=\"pinno\"]")
    WebElement pinNo;

    @FindBy(xpath = "//input[@name=\"telephoneno\"]")
    WebElement phoneNo;

    @FindBy(xpath = "//input[@name=\"emailid\"]")
    WebElement emailId;

    @FindBy(xpath = "//input[@name=\"password\"]")
    WebElement password;

    @FindBy(xpath = "//input[@value=\"Submit\"]")
    WebElement submit;

    @FindBy(xpath = "//input[@value=\"Reset\"]")
    WebElement reset;

    public WebElement getDob() {
        return dob;
    }

    public WebElement getHeading() {
        return heading;
    }

    public WebElement getCustomerName() {
        return customerName;
    }

    public WebElement getMaleGender() {
        return maleGender;
    }

    public WebElement getFemaleGender() {
        return femaleGender;
    }

    public WebElement getAddress() {
        return address;
    }

    public WebElement getCity() {
        return city;
    }

    public WebElement getState() {
        return state;
    }

    public WebElement getPinNo() {
        return pinNo;
    }

    public WebElement getPhoneNo() {
        return phoneNo;
    }

    public WebElement getEmailId() {
        return emailId;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getSubmit() {
        return submit;
    }

    public WebElement getReset() {
        return reset;
    }
}
