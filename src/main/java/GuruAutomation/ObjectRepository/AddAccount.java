package GuruAutomation.ObjectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AddAccount {
        WebDriver driver;

    @FindBy(className = "heading3")
    WebElement heading;

    @FindBy(name = "cusid")
    WebElement customerId;

    @FindBy(name = "selaccount")
    WebElement accountType;

    @FindBy(name = "inideposit")
    WebElement initialDeposit;

    @FindBy(xpath = "//input[@value='submit']")
    WebElement submit;

    @FindBy(xpath = "//input[@value='reset']")
    WebElement reset;


    public AddAccount(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getHeading() {
        return heading;
    }

    public WebElement getCustomerId() {
        return customerId;
    }

    public WebElement getInitialDeposit() {
        return initialDeposit;
    }

    public  void adminViaDropDown(int index) {
        Select dropDown = new Select(accountType);
        dropDown.selectByIndex(index);
    }

    public WebElement getSubmit() {
        return submit;
    }

    public WebElement getReset() {
        return reset;
    }
}
