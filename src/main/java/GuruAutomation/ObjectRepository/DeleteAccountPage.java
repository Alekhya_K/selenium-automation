package GuruAutomation.ObjectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DeleteAccountPage {
    WebDriver driver;

    @FindBy(className = "heading3")
    WebElement heading;

    @FindBy(name = "accountno")
    WebElement accountNo;

    @FindBy(name = "AccSubmit")
    WebElement submit;

    @FindBy(name = "res")
    WebElement reset;

    public DeleteAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }


    public WebElement getHeading() {
        return heading;
    }

    public WebElement getAccountNo() {
        return accountNo;
    }

    public WebElement getSubmit() {
        return submit;
    }

    public WebElement getReset() {
        return reset;
    }
}
