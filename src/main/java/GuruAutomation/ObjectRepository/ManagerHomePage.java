package GuruAutomation.ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManagerHomePage {
    WebDriver driver;
    @FindBy(xpath = "//tbody/tr[3]/td")   //By managerId = By.xpath("//tbody/tr[3]/td"); //alternative
    WebElement managerId;

    @FindBy(linkText = "New Customer")
    WebElement newCustomer;

    @FindBy(linkText = "New Account")
    WebElement newAccount;

    @FindBy(linkText = "Delete Account")
    WebElement deleteAccount;

    public ManagerHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public WebElement getManagerId() {
        return managerId;
    }

    public WebElement addNewCustomer() {
        return newCustomer;
    }

    public WebElement getNewAccount() {
        return newAccount;
    }

    public WebElement getDeleteAccount() {
        return deleteAccount;
    }
}
