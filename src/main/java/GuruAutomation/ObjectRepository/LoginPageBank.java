package GuruAutomation.ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageBank {
    WebDriver driver;

    public LoginPageBank(WebDriver driver) {
        this.driver = driver;
    }

    By userID = By.name("uid");
    By password = By.name("password");
    By loginButton = By.xpath("//input[@value='LOGIN']");

    public WebElement getUserID() {
        return driver.findElement(userID);
    }

    public WebElement getPassword() {
        return driver.findElement(password);
    }

    public WebElement getLoginButton() {
        return driver.findElement(loginButton);
    }
}
