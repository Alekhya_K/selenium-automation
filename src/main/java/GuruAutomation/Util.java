package GuruAutomation;

import GuruAutomation.TestCases.BankProject;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Util {
    public static final String BASE_URL = "http://demo.guru99.com/v4/";
    public static final String USERNAME = "mngr336503";
    public static final String PASSWORD = "vedejet";
    public static final String WEBDRIVER_LOCATION = "C:\\chromedriver.exe";
    //excel parameters
    public static final String FILE_PATH = "C:\\Users\\1646238\\Downloads\\BankProjGuru\\testData.xls";
    public static final String SHEET_NAME = "Data"; // Sheet name
    public static final String TABLE_NAME = "testData"; // Name of data table
    // Expected output
    public static final String EXPECT_TITLE = "Guru99 Bank Manager HomePage";
    public static final String EXPECT_ERROR = "User or Password is not valid";
    public static final String EXPECT_MANAGERID = "mngr336503";
    public static final String SUCCESSFUL_REGISTRATION = "Customer Registered Successfully!!!";
    public static final String ADD_NEW_ACCOUNT = "Add new account form";
    public static final String ACCOUNT_GENERATION_SUCCESS = "Account Generated Successfully!!!";
    public static final String DELETE_ACCOUNT_HEADING = "Delete Account Form";
    //ScreenShots path
    public static final String SCREENSHOT_HOMEPAGE = "C:\\Users\\1646238\\IdeaProjects\\AutomationSelenium\\homepage.png";



    //get Data from test Data
    public static String[][] getDataFromExcel(String xlsFilePath, String xlsSheetName, String xlsTableName) throws BiffException, IOException {
        // Declare a 2 dimensions array to store all the test data read from
        // excel
        String[][] tabArray = null;
        try {

            Workbook workbook = Workbook.getWorkbook(new File(xlsFilePath));
            //sheet name
            Sheet sheet = workbook.getSheet(xlsSheetName);
            // find cell position which contain first appear table name
            Cell tableStart = sheet.findCell(xlsTableName);
            int startRow, startCol, endRow, endCol;
            startRow = tableStart.getRow();
            startCol = tableStart.getColumn();
            Cell tableEnd = sheet.findCell(xlsTableName, startCol + 1, startCol + 1, 100, 6400, false); //doubt
            // Row position of LAST appear table name
            endRow = tableEnd.getRow();
            // Col position of LAST appear table name
            endCol = tableEnd.getColumn();
            tabArray = new String[endRow - startRow - 1][endCol - startCol - 1];
            int ci = 0;
            for (int i = startRow + 1; i < endRow; i++, ci++) {
                int cj = 0;
                for (int j = startCol + 1; j < endCol; j++, cj++) {
                    //Get content of each cell and store to each array element.
                    tabArray[ci][cj] = sheet.getCell(j, i).getContents();

                }
            }
        }
        catch (FileNotFoundException | BiffException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            e.printStackTrace();
        }
        System.out.println("tablarray" +tabArray);
        return tabArray;
    }

    public static void getScreenShot(WebDriver driver) throws IOException {
        System.out.println("inside screenshot");
        File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(src, new File(SCREENSHOT_HOMEPAGE));
    }
}
