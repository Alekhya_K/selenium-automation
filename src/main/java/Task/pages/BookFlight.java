package Task.pages;

import Task.utilities.Utlity;
import org.apache.poi.hssf.record.chart.DatRecord;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import javax.swing.text.Utilities;

public class BookFlight {

    WebDriver driver;
    Utlity utilities = new Utlity();

    @FindBy(id = "flightOrigin1")
    private WebElement from;

    @FindBy(id = "bookFlightDestination")
    private WebElement to;

    @FindBy(id = "departDate1")
    private WebElement departDate;

    @FindBy(xpath = "//label[contains(text(),'One-way')]")
    private WebElement oneWay;

    @FindBy(id = "returnDate1")
    private WebElement returnDate;

    @FindBy(xpath = "//button[contains(text(),'Done')]")
    private WebElement calendarDone;

    @FindBy(id = "flightClass1")
    private WebElement cabinClass;

    @FindBy(id = "flightPassengers1")
    private WebElement passengers;

    @FindBy(xpath = "//button[contains(text(),'Search')]")
    private WebElement search;

    public BookFlight(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void enterSearchDetails(String fromKeys, String toKeys, boolean isOneWay) throws InterruptedException {
        utilities.WebElementSendKeys(from, fromKeys,true);
        Thread.sleep(3000);
        utilities.WebElementSendKeys(to, toKeys, true);
        Thread.sleep(3000);
        departDate.click();

        List<WebElement> dates = driver.findElements(By.xpath("//div[@class='calendar_month_left'] //ul[@class='calendar_days'] " +
                "//li[not(@class='calendar_days--disabled calendar_days--invalid')]"));
        dates.stream().forEach(i->System.out.println(i.getText()));

        int currentDay = LocalDateTime.now().getDayOfMonth();
        for(WebElement element : dates) {
            //set the date as current date
            if(Integer.parseInt(element.getText().trim()) == currentDay) {
                ((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
                //element.click();
                break;
            }
        }

        if(!isOneWay) {
           //logic for setting return date
        }
        else {
            oneWay.click();
        }
        Thread.sleep(3000);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", calendarDone);
        search.click();

    }




}
