package Task.utilities;

import Task.testCases.BaseClass;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.NoSuchElementException;

public class Utlity extends BaseClass {
    public static Actions actions = null;
    public static Select select = null;

    public  void WebElementSendKeys(WebElement element, String keys, boolean clear) {
        if (element != null) {
            try {
                if (clear) {
                    element.clear();
                }
                if (keys != null) {
                    element.sendKeys(keys);
                }
            }
            catch (NoSuchElementException | ElementNotVisibleException | ElementNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    public  void WebElementHoverOrClick(WebElement element, boolean moveAndClick, boolean justHover) {
        if(element != null) {
            actions = new Actions(driver);
            try {
                if(justHover) {
                    actions.moveToElement(element).build().perform();
                }

                else if(moveAndClick) {
                    actions.moveToElement(element).click();
                }
            }
            catch (NoSuchElementException | ElementNotVisibleException | ElementNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public  void SelectfromDropDown(WebElement element, String value) {
        if(element != null) {
            try {
                select = new Select(element);
                select.selectByVisibleText(value);
            }
            catch (NoSuchElementException | ElementNotVisibleException | ElementNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

}
