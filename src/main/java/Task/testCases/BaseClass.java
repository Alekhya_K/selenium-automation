package Task.testCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseClass {
    public WebDriver driver;
    Properties properties = null;
    String browser = null;

    public void loadProperty() {
        try {
            FileInputStream inputStream = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\java\\Task\\resources\\config.properties");
            properties = new Properties();
            properties.load(inputStream);
            browser = properties.getProperty("browser");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @BeforeSuite
    public void setUp() {
        loadProperty();
        if(browser != null) {
            if(browser.equalsIgnoreCase("chrome")) {
                System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe");
                driver = new ChromeDriver();
            }
        }
        if(driver != null) {
            driver.get(properties.getProperty("baseUrl"));
            System.out.println(properties.getProperty("baseUrl"));
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        }
    }

    @AfterSuite
    public void tearDown() {
        driver.quit();
    }






}
