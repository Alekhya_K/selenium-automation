package Miscellaneous;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class HttpsCertificate {
    public static void main(String args[]) {
        //customizing the browser
        DesiredCapabilities dc= DesiredCapabilities.chrome();
        dc.acceptInsecureCerts();
        //dc.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS,true);
        //add it to your local browser
        ChromeOptions c= new ChromeOptions();
        c.merge(dc);
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver(c);

    }
}
