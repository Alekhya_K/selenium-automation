package Miscellaneous;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BrokenLinks {
    public static void main(String args[]) throws IOException {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
        driver.manage().window().maximize();
        List<WebElement> links = driver.findElements(By.tagName("a"));
        List<String> linksList = new ArrayList<>();
        links.forEach(i -> {
            linksList.add(i.getAttribute("href"));
        });
        for(String link: linksList) {
            HttpURLConnection urlConnection = (HttpURLConnection) new URL(link).openConnection();
            urlConnection.setRequestMethod("HEAD");
            urlConnection.connect();
            if(urlConnection.getResponseCode() <= 400) {
                System.out.println("no issue");
            }
        }
    }
}
