package FlightBooking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;


//Automation spice jet website home page entering details like org dest date etc and clicking on search
public class SpiceJetAutomation {
    public static void main(String args[]) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.spicejet.com/");
        Thread.sleep(4000);
        //from,to dropdown
        driver.findElement(By.xpath("//input[@id='ctl00_mainContent_ddl_originStation1_CTXT']")).click();
        driver.findElement(By.xpath("//a[contains(text(),'Chennai (MAA)')]")).click();
        Thread.sleep(6000);
        driver.findElement(By.xpath("//div[@id='glsctl00_mainContent_ddl_destinationStation1_CTNR']//a[text() =' Ahmedabad (AMD)']")).click();

        //calendar
        Thread.sleep(3000);
        driver.findElement(By.cssSelector(".ui-state-default.ui-state-highlight.ui-state-active")).click();

        //passenger drop down
        driver.findElement(By.id("divpaxinfo")).click();
        WebElement dropdown1 = driver.findElement(By.id("ctl00_mainContent_ddl_Adult"));
        Select option1 = new Select(dropdown1);
        option1.selectByValue("3");
        WebElement dropdown2 = driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_Child']"));
        Select option2 = new Select(dropdown2);
        option2.selectByValue("1");

        //currency drop down
        WebElement dropdown = driver.findElement(By.xpath("//select[@id='ctl00_mainContent_DropDownListCurrency']"));
        Select options = new Select(dropdown);
        options.selectByVisibleText("INR");

        //checkbox
        driver.findElement(By.id("ctl00_mainContent_chk_friendsandfamily")).click();

        //search button
        driver.findElement(By.id("ctl00_mainContent_btn_FindFlights")).click();

    }
}
