package FlightBooking;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import sun.security.provider.certpath.OCSPResponse;

public class FlightBooking {
    public static void main(String args[]) {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        //Step1 :create driver object
        WebDriver chromeDriver = new ChromeDriver();
        chromeDriver.get("https://rahulshettyacademy.com/dropdownsPractise/#");
        System.out.println(chromeDriver.getTitle());
        //drop down with select tag
        WebElement staticDropDown = chromeDriver.findElement(By.xpath("//select[@id='ctl00_mainContent_DropDownListCurrency']"));
        Select dropdown = new Select(staticDropDown);
        dropdown.selectByIndex(1); //select by index
        //select by visible text
        dropdown.selectByVisibleText("AED");
        dropdown.selectByValue("USD");
        for(WebElement webElement : dropdown.getAllSelectedOptions()) {
            System.out.println(webElement.getText());
        }

    }
}
