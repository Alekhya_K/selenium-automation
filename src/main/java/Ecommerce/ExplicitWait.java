package Ecommerce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ExplicitWait {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        WebDriverWait explicitWait = new WebDriverWait(driver,5);
        driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
        List<String> vegetables = Arrays.asList("Cucumber","Tomato","Beetroot");
        Thread.sleep(2000);
        ExplicitWait wait = new ExplicitWait();
        wait.addItems(driver,vegetables);
        driver.findElement(By.xpath("//img[@src=\"https://res.cloudinary.com/sivadass/image/upload/v1493548928/icons/bag.png\"]")).click();
        driver.findElement(By.xpath("//button[contains(text(),'PROCEED TO CHECKOUT')]")).click();
        explicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@class=\"promoCode\"]")));
        driver.findElement(By.xpath("//input[@class=\"promoCode\"]")).sendKeys("rahulshettyacademy");
        driver.findElement(By.xpath("//button[contains(text(),'Apply')]")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Place Order')]")).click();
        WebElement webElement = driver.findElement(By.tagName("select"));
        Select select = new Select(webElement);
        select.selectByValue("India");
        driver.findElement(By.xpath("//input[@type='checkbox']")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Proceed')]")).click();

    }
    void addItems(WebDriver driver, List<String> vegetables) {
        String name;
        int count=0;
        List<WebElement> products = driver.findElements(By.cssSelector("h4.product-name"));
        for(int i=0; i<products.size() ; i++) {
            name = products.get(i).getText().split(" ")[0];
            if (vegetables.contains(name)) {
                count++;
                System.out.println(name+" "+i);
                //button value is changing dynamically
                //System.out.println("size"+driver.findElements(By.xpath("//button[text()='ADD TO CART']")).size());
                driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();
                if(count==vegetables.size()){
                    break;
                }
            }
        }
    }
}
