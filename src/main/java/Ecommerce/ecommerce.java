package Ecommerce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class ecommerce {
    public static void main(String args[]) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver","C:\\chromedriver.exe"); //location of chrome driver
        WebDriver driver = new ChromeDriver();
        driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
        Thread.sleep(2000);
        List<String> vegetables = new ArrayList<>();
        vegetables.add("Cucumber");
        vegetables.add("Tomato");
        vegetables.add("Beetroot");
        String name;
        int count=0;
        List<WebElement> products = driver.findElements(By.cssSelector("h4.product-name"));
        for(int i=0; i<products.size() ; i++) {
                name = products.get(i).getText().split(" ")[0];
                if (vegetables.contains(name)) {
                    count++;
                    System.out.println(name+" "+i);
                    //System.out.println("size"+driver.findElements(By.xpath("//button[text()='ADD TO CART']")).size());
                    driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();
                    if(count==vegetables.size()){
                        break;
                    }
                    //Thread.sleep(5000);
                }

           // Thread.sleep(4000);

        }
        driver.findElement(By.xpath("//img[@src=\"https://res.cloudinary.com/sivadass/image/upload/v1493548928/icons/bag.png\"]")).click();
        Thread.sleep(3000); //to solve this refer wait class(Implicit wait method)
        driver.findElement(By.xpath("//button[contains(text(),'PROCEED TO CHECKOUT')]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//input[@class=\"promoCode\"]")).sendKeys("rahulshetty");
        driver.findElement(By.xpath("//button[contains(text(),'Apply')]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//button[contains(text(),'Place Order')]")).click();
        WebElement webElement = driver.findElement(By.tagName("select"));
        Select select = new Select(webElement);
        select.selectByValue("India");
        driver.findElement(By.xpath("//input[@type='checkbox']")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Proceed')]")).click();


    }
}
