import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxWebdriver {
    public static void main(String args[]) {
        System.setProperty("webdriver.gecko.driver","C:\\geckodriver.exe"); //location of chrome driver
        //Step1 :create driver object
        WebDriver firefoxWebdriver = new FirefoxDriver();
        firefoxWebdriver.get("https://en-gb.facebook.com/");
        System.out.println(firefoxWebdriver.getTitle());
        //firefoxWebdriver.close();

        //Strictly implements methods of Webdriver interface


    }
}
